#!/usr/bin/python

import sys
import serial 
import time

# Configuration

GPS_PORT = "/dev/gps"
GPS_BAUDRATE = 9600
SONAR_PORT = "/dev/sonar"
SONAR_BAUDRATE = 4800

GMT= +2

DATAFILE = ""

# variables
gps_valid = False
filename_set = False
datafile = ""

depth = "0"
temp = "25"

# Get parameters

if len(sys.argv) > 1:
    DATAFILE = sys.argv[1]
    #print(DATAFILE)
else:
    DATAFILE = "data.txt"


# Ports

gps = serial.Serial(GPS_PORT, 9600, timeout=1)
sonar = serial.Serial(SONAR_PORT, SONAR_BAUDRATE, timeout=1)

# main loop
while 1:
    gps_line = gps.readline().decode('ascii')

    if gps_line.split(",")[0] == "$GPGGA":
        gpgga = gps_line.split(",")
        gpstime = gpgga[1]
        if gpstime != "":
            gpstime = str(int(gpstime[0:2])+GMT)+":"+gpstime[2:4]+":"+gpstime[4:6]
        lat = gpgga[2]
        ns = gpgga[3]
        lon = gpgga[4]
        we = gpgga[5]
        fix = gpgga[6]
        sats = gpgga[7]

        print ("time: " + gpstime)
        print ("sats: " + sats)

        if int(sats) > 3:
            gps_valid = True

        time.sleep(0.9)

        # append timestamp to filename and open file
        if gps_valid == True and filename_set == False:
            filename_set = True
            DATAFILE = DATAFILE + "-" + gpstime
            datafile = open(DATAFILE, 'w')

        # write data to file if valid data is present
        if filename_set == True and gps_valid == True:
            
            # check sonar
            sonar_line = sonar.readline().decode('ascii')
            # depth
            while sonar_line.split(",")[0] != "$SDDPT":
                sonar_line = sonar.readline().decode('ascii')
            sddpt = sonar_line.split(",")
            depth = sddpt[1]
            if depth=="":
                depth = "x"

            # temp
            while sonar_line.split(",")[0] != "$SDMTW":
                sonar_line = sonar.readline().decode('ascii')
            sdmtw = sonar_line.split(",")
            temp = sdmtw[1]
            if temp=="":
                temp = "x"


            datafile.write(gpstime + "\t" + lat + "\t" + ns + "\t")
            datafile.write(lon + "\t" + we + "\t" + depth + "\t" + temp + "\n")
            datafile.flush()

