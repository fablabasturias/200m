#!/bin/sh

case "$1" in

	start)
		sudo killall wpa_supplicant
		sudo ifconfig wlan0 192.168.23.1
		sudo service isc-dhcp-server start
		sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
		sudo hostapd -B -P /var/run/hostapr-pid /etc/hostapd/hostapd.conf 
		sleep 2
		sudo ifconfig wlan0 192.168.23.1
		;;
	stop)
		sudo killall hostapd
		sudo service isc-dhcp-server stop
		sudo wpa_supplicant -Dwext -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant.conf&
		sleep 3
		sudo dhclient wlan0
		;;
	*)
		echo "configura start | stop"
		echo
		;;
esac

