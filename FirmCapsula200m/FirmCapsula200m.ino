/*
 *
 * 200 metros
 * FirmCapsula200m: Firmware para la cápsula
 * Luis Díaz, fabLAB Asturias
 * LABoral Centro de Arte y Creacion Industrial
 * 
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this code.  If not, see <http://www.gnu.org/licenses/>.
 *
 */




#include <DS3232RTC.h>
#include <Wire.h>
#include <SparkFunISL29125.h>
#include <SPI.h>
#include <SD.h>
#include <Time.h>

SFE_ISL29125 RGB_sensor;

File dataFile;

int mps = 5; //muestras por segundo

unsigned int red = 0;
unsigned int green = 0;
unsigned int blue = 0;

const int chipSelect = 10;
 
char filename[] = "LOGGER00.TXT";

void setup() {
  
  Serial.begin(115200);
  setSyncProvider(RTC.get);   // the function to get the time from the RTC

  // Initialize the ISL29125 with simple configuration so it starts sampling
  if (RGB_sensor.init())
  {
    Serial.println("Sensor Initialization Successful\n\r");
    
    while (!SD.begin(chipSelect)) {
      Serial.println("Card failed, or not present");
      delay(3000);
    }
    Serial.println("card initialized.");
  }
  
  for (uint8_t i = 0; i < 100; i++) {
    filename[6] = i/10 + '0';
    filename[7] = i%10 + '0';
    if (! SD.exists(filename)) {
      break;  // leave the loop!
    }
  }
}

void loop() {
  readRGB();
  dataFile = SD.open(filename, FILE_WRITE);
  if (dataFile) writeToSd(dataFile);
  else dataFile.close();
  delay(1000/mps);
}

void readRGB(){
  red = RGB_sensor.readRed();
  green = RGB_sensor.readGreen();
  blue = RGB_sensor.readBlue();
}

void writeToSd(File archivo){
  
  //FECHA
  archivo.print(day());
  archivo.print("\t");
  archivo.print(month());
  archivo.print("\t");
  archivo.print(year());
  archivo.print("\t");
  archivo.print(hour());
  archivo.print("\t");  
  archivo.print(minute());
  archivo.print("\t");
  archivo.print(second());
  archivo.print("\t");
  //VALORES DEL SENSOR
  archivo.print(red);
  archivo.print("\t");
  archivo.print(green);
  archivo.print("\t");
  archivo.print(blue);
  archivo.println();
  dataFile.close();
    
}
